﻿
//

#include "new_features.h"

using namespace std;



string test_string_view(std::string& input, int& size) { //Store pointer + size, not the character 
	auto s = input; // same as string("ABCDE")

	std::string_view b(s.data(), size);

	std::string str{ b };
	auto output = str;

	return output;

}


struct Point {
	Point(int x_, int y_, int z_)
		: x(x_)
		, y(y_)
		, z(z_)
	{}
	int x, y, z;
};

//[[deprecated]]
Point getPoint() {
	return Point(1, 2, 4);
}

//Template Class
template<class T>
class TestData {
public:
	TestData(T v_) : v(v_) {} // constructor
	T v;
};

template<typename T>
TestData<T> create_TestData(T i) {
	return TestData<T>(i);
}

void test_constructor_template_deduction() {

	std::unique_ptr<Point>p(new Point(1, 3, 5));
	print_out(p->x);
	print_out(p->y);
	print_out(p->z);
	//TestData< std::vector<std::unique_ptr<TestData<int> > >> (10);

	auto x = TestData<int>(10);
	auto y = create_TestData(10);


	print_out(x.v);
	print_out(y.v);

}


constexpr int constexpr_loop(int num = 4) {
	int sum = 0.0;
	for (int i = 1.0; i <= num; i++) {
		sum += i;
		print_out(sum);
	}
	return sum;
}

void test_constexpr() {
	print_out(constexpr_loop());
}

void test_fallthrough() {
	int i = 0;

	(constexpr_loop() > 5.0) ? i = 0 : i = 1;

	switch (i) {
	case 0:
		print_out("case 0");
		// gcc -Wimplicit-fallthrough
		//break; // forgot break // SHOW warining?! switch no break
	case 1:
		print_out("case 1");
		[[fallthrough]]; // against the fallthrough warning, NEW attribute
	case 2:
		print_out("case 1");
		break;

	}
}


int main()
{

	auto input = std::string("ABCD");
	int size = 3;
	auto output = test_string_view(input, size);
	//run_test(test_string_view(input));
	print_out(output);


	test_constructor_template_deduction();
	test_constexpr();
	test_fallthrough();

	return 0;
}